{#
SPDX-FileCopyrightText: 2023 Gerald Wiese <wiese@gnuhealth.org>
SPDX-FileCopyrightText: 2008-2023 Luis Falcón <falcon@gnuhealth.org>
SPDX-FileCopyrightText: 2011-2023 GNU Solidario <health@gnusolidario.org>

SPDX-License-Identifier: GPL-3.0-or-later
#}

#!/bin/bash

{{ gh_template_comment | comment }}

VERSION=$({{ gh_venv }}/bin/pip3 show gnuhealth-all-modules | grep Version | cut -b 10-)
TRANSLATE_URL="https://hosted.weblate.org"

usage()
{
    cat << EOF

This is GNU Health control center ${VERSION}

usage: `basename $0` command [options]

Command:

  version    : Show version
  status     : Show environment and GNU Health Tryton server status
  backup     : Backup config & attachments directory, database and pip environment
  update     : Update using pip
  getlang    : Get and install / update the language pack code

Options:

 --backdir  : destination directory, mandatory argument for backup
 --database : database name to use, mandatory argument for backup

EOF
    exit 0
}

help()
{
    cat << EOF
    The GNU Health Control Center (gnuhealth-control) was the main tool for 
    administrative tasks of the GNU Health environment.

    If using Ansible for deployment it is recommended to use Ansible for backups as well.
    Besides this script will only work if the database server is on the same system as the application server.
    Nevertheless the functionality of gnuhealth-control is adapted to the Ansible deployment for backwards compatibility.

    Updates
    -------

    When gnuhealth-control is invoked with the update command, 
    it will update GNU Health components within the same major number
    The following components will be checked and updated if necessary

        - Trytond : Tryton server version
        - GNU Health patchsets

    This will be valid for version with the same major and minor numbers, for example
    4.4.x will look for the latest tryton updates and GNU Health updates
    associated to that release.


EOF
    usage
    exit 0
}

message()
{
    local UTC="$(date -u +'%Y-%m-%d %H:%M:%S')"

    case $1 in
      ERROR ) echo -e "\e[00;31m${UTC} [ERROR] $2\e[00m";;
      WARNING ) echo -e "\e[0;33m${UTC} [WARNING] $2\e[m" ;;
      INFO ) echo -e "\e[0;36m${UTC} [INFO] $2\e[m" ;;
    esac
}

do_update() {
	GNUHEALTH_VERSION_CURRENT=$VERSION
	a=$({{ gh_venv }}/bin/pip3 show gnuhealth-all-modules | grep Version | cut -b 10- | cut -d . -f 1)
	b=$({{ gh_venv }}/bin/pip3 show gnuhealth-all-modules | grep Version | cut -b 10- | cut -d . -f 2)
	c=$((b + 1))
	NEXT_MAJOR="${a}.${c}"
	echo 'Current GNU Health version:'
	echo $GNUHEALTH_VERSION_CURRENT
	echo "Updating PyPI package gnuhealth-all-modules < $NEXT_MAJOR"
	{{ gh_venv }}/bin/pip3 install --upgrade "gnuhealth-all-modules<$NEXT_MAJOR,>=$GNUHEALTH_VERSION_CURRENT"
	GNUHEALTH_VERSION_NEW=$({{ gh_venv }}/bin/pip3 show gnuhealth-all-modules | grep Version | cut -b 10-)
	echo 'New GNU Health version:'
	echo $GNUHEALTH_VERSION_NEW
	if [ "$GNUHEALTH_VERSION_CURRENT" == "$GNUHEALTH_VERSION_NEW" ]; then
		echo 'GNU Health was already up to date.'
	else
		echo 'GNU Health was updated. Run "sudo systemctl restart gnuhealth" to restart the service and have the updates in place.' | tee -a {{ gh_log_path }}/gnuhealth.log
	fi
}

do_backup() {

    local BACKDATE=`date -u +%Y-%m-%d_%H%M%S`
    local LOCKFILE="{{ gh_run_path }}/gnuhealth_backup.lock"
    local INFOFILE="{{ gh_log_path }}/gnuhealth.log"

	shift # Remove the command and deal only with the options

    if [ $# -ne 4 ]; then
        echo -e "Usage : gnuhealth-control backup --backdir <directory> --database <dbname>"
        exit
    fi

    for option in "$@"
    do
      case $option in
          --backdir ) BACKDIR=${2%/};;
          --database ) DB=$2 ;;
      esac
      shift
    done

    if [ -f $LOCKFILE ]
    then
        message "ERROR" "Backup in progress or stale lock file found ..." | tee -a $INFOFILE
        bailout
    fi

    if [ ! -e ${BACKDIR} ]
    then
        message "ERROR" "Backup directory ${BACKDIR} not found !"
        bailout
    fi

    echo $$ > $LOCKFILE 

    # Backup start

    message "INFO" "Backing up operating system info" | tee -a $INFOFILE

    cp /etc/os-release $BACKDIR/os-release_$BACKDATE || bailout

    message "INFO" "Backing up config files" | tee -a $INFOFILE

    tar -cvzf $BACKDIR/config_$BACKDATE.tar.gz {{ gh_config_path }} || bailout

    message "INFO" "Backing up attachments" | tee -a $INFOFILE

    tar -cvzf $BACKDIR/attachments_$BACKDATE.tar.gz {{ gh_data_path }} || bailout

    message "INFO" "Backing up pip environment" | tee -a $INFOFILE

    {{ gh_venv }}/bin/pip3 freeze -v > $BACKDIR/pip_freeze_$BACKDATE.txt || bailout

    if [ -f {{ gh_venv }}/pyvenv.cfg ]; then
      cp {{ gh_venv }}/pyvenv.cfg $BACKDIR/pyvenv_$BACKDATE.cfg || bailout
    fi

    message "INFO" "Getting database dump" | tee -a $INFOFILE

    pg_dump -d $DB > $BACKDIR/database_dump_$BACKDATE.sql || bailout

    message "INFO" "Compressing database dump" | tee -a $INFOFILE

    gzip -v $BACKDIR/database_dump_$BACKDATE.sql || bailout

    message "INFO" "Backup Successful" | tee -a $INFOFILE

    #Remove lock file
    rm $LOCKFILE

}

getlang() {
    if [ $# -eq 1 ]; then
        usage
    fi

    if [ ! command -v msgcat >/dev/null 2>&1 ]; then
        echo "msgcat command is not found, please install gettext package."
        exit 1
    fi

    local lang_to_install=$2
    local lang_file=${lang_to_install}.zip
    source {{ gh_rc_path }} || bailout
    local lang_download_dir=$(mktemp -d /tmp/gnuhealth-XXXX)
    TEMP_STRING=$({{ gh_venv }}/bin/pip3 show gnuhealth-all-modules | grep Location)
    GNUHEALTH_DIR=${TEMP_STRING:10}
    local gnuhealth_module_dir=${GNUHEALTH_DIR}/trytond/modules

    message "INFO" "Retrieving language pack file for ${lang_to_install}"
    wget ${TRANSLATE_URL}/download/gnu-health/-/${lang_to_install}/?format=zip -O ${lang_download_dir}/${lang_file} || bailout

    cd ${lang_download_dir} || bailout
    bsdtar --strip-components 3 -xzf ${lang_download_dir}/${lang_file} || bailout

    message "INFO" "Installing / Updating language files for ${lang_to_install} ..."

    ## After gnuhealth-4.4, we use po-export tool to update po and pot
    ## files of gnuhealth, po files will sync to
    ## gnuhealth-hg-dev. which means that po items used in before
    ## gnuhealth version but removed in gnuhealth-hg-dev will be
    ## removed and not sync to weblate to translate.
    ##
    ## To solve this problem, we use msgcat to merge weblate's po
    ## files and installed po files, but removed po items can not
    ## lost, but they will be not updated with weblate, user should
    ## maintain them by manual.

    for po in $(find . -name '*.po'); do
        local weblate_po=${lang_download_dir}/${po}
        local installed_po=${gnuhealth_module_dir}/${po}
        local installed_pot=${gnuhealth_module_dir}/$(echo ${po} | awk -F/ '{print $2 "/locale/" $2 ".pot"}')

        ## Sometimes, po files will include duplicate strings, deal
        ## with it for msgcat.
        msguniq --use-first ${weblate_po} -o ${weblate_po}

        if [ -e ${installed_po} ]; then
            msguniq --use-first ${installed_po} -o ${installed_po}
            msgcat --use-first ${weblate_po} ${installed_po} -o ${installed_po}
        else
            if [ -e ${installed_pot} ]; then
                msguniq --use-first ${installed_pot} -o ${installed_pot}
                msgcat --use-first ${weblate_po} ${installed_pot} -o ${installed_po}
            else
                msgcat --use-first ${weblate_po} -o ${installed_po}
            fi
        fi
    done

    message "INFO" "Language pack ${lang_to_install} sucessfully installed / Updated"
    message "INFO" "You now need to load language translations again in one the following ways:"
    message "INFO" "1. Run command (have no effect sometimes)"
    message "INFO" "   1. cdexe"
    message "INFO" "   2. ./tryton-admin --database YOUR-DB --all --language YOUR-LANG"
    message "INFO" "2. Open language form: Administration > Localization > Languages > YOUR-LANG"
    message "INFO" "   Click 'Unload translations' then click 'Load translations'"
    message "INFO" "Removing temporary directories"

    cd ${gnuhealth_module_dir} || bailout
    rm -rf ${lang_download_dir}
    cd
}


bailout() {
    message "ERROR" "Bailing out !" | tee -a $INFOFILE
    message "ERROR" "Removing lock file" | tee -a $INFOFILE
    rm -f $LOCKFILE
    exit 1
}

parse_command_line()
{
    if [ $# -eq 0 ]; then
        usage
    fi

    case $1 in
        version) echo $VERSION;;
        backup) do_backup $@;;
        update) do_update $@;;
        status) systemctl status gnuhealth;;
        getlang) getlang $@;;
        help) help;;
        *) echo $1: Unrecognized command; exit 1;;
    esac
}

parse_command_line $@
