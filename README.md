<!--
SPDX-FileCopyrightText: 2023 Gerald Wiese

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Migrated to Codeberg
**This project was migrated to the new GNU Health repositories on Codeberg:**

https://codeberg.org/gnuhealth/ansible/src/branch/main/roles/gnuhealth



# GNU Health Ansible role

This role installs the GNU Health Hospital Information System (HIS node).

It expects an existing PostgreSQL database set as gh_psql_db_name owned by the PostgreSQL user defined in gh_psql_db_user.

Furthermore the handler triggers to restart the systemd service gnuhealth without creating it as this should be handled by uWSGI.
