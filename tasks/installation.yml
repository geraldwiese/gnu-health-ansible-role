# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

- name: Install packages
  ansible.builtin.package:
    name: "{{ gh_packages[ansible_facts.os_family] }}"
- name: Install newer python3 and python rpm
  ansible.builtin.package:
    name: "{{ gh_py_suse_packages }}"
  when: gh_py_suse_upgrade and ansible_facts.os_family == "Suse"

- name: Get package facts
  ansible.builtin.package_facts:
- name: Install LibreOffice on Debian
  ansible.builtin.apt:
    name: "{{ gh_libreoffice_packages[ansible_facts.os_family] }}"
    install_recommends: "{{ gh_install_libreoffice_recommends }}"
  when:
    - gh_install_libreoffice
    - ansible_facts.os_family == "Debian"
    - "'libreoffice-writer' not in ansible_facts.packages and 'libreoffice-calc' not in ansible_facts.packages"
- name: Install LibreOffice on SUSE
  community.general.zypper:
    name: "{{ gh_libreoffice_packages[ansible_facts.os_family] }}"
    disable_recommends: "{{ not gh_install_libreoffice_recommends }}"
  when:
    - gh_install_libreoffice
    - ansible_facts.os_family == "Suse"
    - "'libreoffice-writer' not in ansible_facts.packages and 'libreoffice-calc' not in ansible_facts.packages"
- name: Install LibreOffice on FreeBSD
  ansible.builtin.package:
    name: "{{ gh_libreoffice_packages[ansible_facts.os_family] }}"
  when:
    - gh_install_libreoffice
    - ansible_facts.os_family == "FreeBSD"

- name: Add gnuhealth user
  ansible.builtin.user:
    name: gnuhealth
    shell: /sbin/nologin
    password_lock: true
    create_home: true
    home: "{{ gh_home_path }}"

- name: Create directories for gnuhealth
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    owner: gnuhealth
    group: "{{ 'users' if ansible_facts.os_family == 'Suse' else 'gnuhealth' }}"
    mode: "{{ item.mode }}"
  loop:
    - {path: '{{ gh_home_path }}', mode: '0755'}
    - {path: '{{ gh_home_path }}/.ansible/tmp', mode: '0700'}
    - {path: '{{ gh_config_path }}', mode: '0750'}
    - {path: '{{ gh_log_path }}', mode: '0755'}
    - {path: '{{ gh_data_path }}', mode: '0750'}
    - {path: '{{ gh_tls_path }}', mode: '0750'}

- name: Copy certificate
  ansible.builtin.copy:
    src: '{{ gh_cert.src }}'
    dest: '{{ gh_cert.dest }}'
    owner: gnuhealth
    group: "{{ 'users' if ansible_facts.os_family == 'Suse' else 'gnuhealth' }}"
    remote_src: true
    mode: '0640'
  when: gh_https
- name: Copy key
  ansible.builtin.copy:
    src: '{{ gh_key.src }}'
    dest: '{{ gh_key.dest }}'
    owner: gnuhealth
    group: "{{ 'users' if ansible_facts.os_family == 'Suse' else 'gnuhealth' }}"
    mode: '0600'
    remote_src: true
  no_log: true
  when: gh_https

- name: Installation steps as 'gnuhealth' user
  become_user: gnuhealth
  become: true
  become_method: "{{ gh_become_method_unprivileged_users }}"
  become_exe: "{{ gh_become_exe_unprivileged_users }}"
  become_flags: "{{ gh_become_flags_unprivileged_users }}"
  when: "ansible_facts.os_family != 'FreeBSD'"
  notify: Restart and enable gnuhealth
  block:
    - name: Verify PostgreSQL certificate
      when: gh_psql_verify_tls
      block:
        - name: Create postgresql directory for cert validation
          ansible.builtin.file:
            path: "{{ gh_tls_path }}/postgresql"
            state: directory
            mode: '0750'
        - name: Copy certificate
          ansible.builtin.copy:
            src: "{{ gh_psql_cert_path }}"
            dest: "{{ gh_tls_path }}/postgresql/root.crt"
            mode: '0640'
        - name: Copy CRL
          ansible.builtin.copy:
            src: "{{ gh_psql_crl_path }}"
            dest: "{{ gh_tls_path }}/postgresql/root.crl"
            mode: '0640'
          when: gh_psql_use_crl
        - name: Ensure postgresql CRL exists
          ansible.builtin.copy:
            content: ""
            dest: "{{ gh_tls_path }}/postgresql/root.crl"
            force: false
            mode: '0640'

    - name: Install gnuhealth using pip, create venv
      ansible.builtin.pip:
        name: "gnuhealth-all-modules=={{ gh_version }}"
        virtualenv: "{{ gh_venv }}"
        virtualenv_command: "{{ gh_py_suse_exe if ansible_facts.os_family == 'Suse' else 'python3' }} -m venv"
        extra_args: "{{ '--index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/' if gh_use_testpypi else '' }}"
    - name: Get gnuhealth location
      ansible.builtin.shell: 'set -o pipefail && {{ gh_venv }}/bin/pip3 show gnuhealth | grep Location'
      args:
        executable: /bin/bash
      register: gh_location
      changed_when: false
    - name: Apply gnuhealth config template
      ansible.builtin.template:
        src: trytond.conf.j2
        dest: "{{ gh_config_path }}/trytond.conf"
        mode: '0640'
    - name: Apply log config template
      ansible.builtin.template:
        src: gnuhealth_log.conf.j2
        dest: "{{ gh_config_path }}/gnuhealth_log.conf"
        mode: '0640'
    - name: Apply gnuhealthrc template
      ansible.builtin.template:
        src: gnuhealthrc.j2
        dest: "{{ gh_rc_path }}"
        mode: '0640'
    - name: Load .gnuhealthrc from .bashrc
      ansible.builtin.lineinfile:
        path: "{{ gh_home_path }}/.bashrc"
        line: "source {{ gh_rc_path }}"
    - name: Check if database is initialized (pw set)
      ansible.builtin.expect:
        command: /bin/bash -c 'psql -d {{ gh_psql_db_name }} -U {{ gh_psql_db_user }} -h {{ gh_psql_domain }} -c "\dt" | xargs echo'
        responses:
          (.*)Password(.*): "{{ gh_psql_pw }}"
      no_log: true
      changed_when: false
      register: gh_db_tables
      when: gh_psql_use_pw
    - name: Check if database is initialized
      ansible.builtin.shell: "set -o pipefail && psql -d {{ gh_psql_db_name }} -U {{ gh_psql_db_user }} -c '\\dt' | xargs echo"
      args:
        executable: /bin/bash
      changed_when: false
      register: gh_db_tables_alt
      when: not gh_psql_use_pw
    - name: Set boolean db init
      ansible.builtin.set_fact:
        gh_db_init: "{{ 'res_user' in gh_db_tables.stdout if gh_psql_use_pw else 'res_user' in gh_db_tables_alt.stdout }}"
    - name: Initialize database in tryton
      ansible.builtin.expect:
        command: '{{ gh_venv }}/bin/trytond-admin -c {{ gh_config_path }}/trytond.conf -d {{ gh_psql_db_name }} --all'
        chdir: "{{ gh_location.stdout[10:] }}"
        responses:
          (.*)email(.*): "{{ gh_tryton_admin_mail }}"
          (.*)password for(.*): "{{ gh_tryton_pw }}"
          (.*)password confirmation(.*): "{{ gh_tryton_pw }}"
        timeout: 300
      no_log: true
      when: not gh_db_init

    - name: List modules
      ansible.builtin.shell:
        cmd: |
          {{ gh_venv }}/bin/trytond-console -d {{ gh_psql_db_name }} -c {{ gh_config_path }}/trytond.conf << EOF
          Module = pool.get('ir.module')
          modules = Module.search([('state', '=', 'activated')])
          for module in modules:
            print(module.name)
          EOF
        chdir: "{{ gh_location.stdout[10:] }}"
      register: gh_trytond_modules
      changed_when: false
    - name: Activate modules
      ansible.builtin.command: >
        {{ gh_venv }}/bin/trytond-admin -c {{ gh_config_path }}/trytond.conf -d {{ gh_psql_db_name }}
        -u {{ item }} --activate-dependencies
      args:
        chdir: "{{ gh_location.stdout[10:] }}"
      loop: "{{ gh_modules }}"
      when: item not in gh_trytond_modules.stdout
      changed_when: true
    - name: List modules again after activation
      ansible.builtin.shell:
        cmd: |
          {{ gh_venv }}/bin/trytond-console -d {{ gh_psql_db_name }} -c {{ gh_config_path }}/trytond.conf << EOF
          Module = pool.get('ir.module')
          modules = Module.search([('state', '=', 'activated')])
          for module in modules:
            print(module.name)
          EOF
        chdir: "{{ gh_location.stdout[10:] }}"
      register: gh_trytond_modules
      changed_when: false
    - name: Get number of imported countries
      ansible.builtin.shell:
        cmd: |
          {{ gh_venv }}/bin/trytond-console -d {{ gh_psql_db_name }} -c {{ gh_config_path }}/trytond.conf << EOF
          Country = pool.get('country.country')
          countries = Country.search([])
          print(len(countries))
          EOF
        chdir: "{{ gh_location.stdout[10:] }}"
      register: gh_trytond_countries
      changed_when: false
    - name: Import trytond countries
      ansible.builtin.command: "{{ gh_venv }}/bin/trytond_import_countries -c {{ gh_config_path }}/trytond.conf -d {{ gh_psql_db_name }}"
      when: "'country' in gh_trytond_modules.stdout and gh_trytond_countries.stdout == '0'"
      changed_when: true
    - name: Get number of imported currencies
      ansible.builtin.shell:
        cmd: |
          {{ gh_venv }}/bin/trytond-console -d {{ gh_psql_db_name }} -c {{ gh_config_path }}/trytond.conf << EOF
          Currency = pool.get('currency.currency')
          currencies = Currency.search([])
          print(len(currencies))
          EOF
        chdir: "{{ gh_location.stdout[10:] }}"
      register: gh_trytond_currencies
      changed_when: false
    - name: Import trytond currencies
      ansible.builtin.command: "{{ gh_venv }}/bin/trytond_import_currencies -c {{ gh_config_path }}/trytond.conf -d {{ gh_psql_db_name }}"
      when: "'currency' in gh_trytond_modules.stdout and gh_trytond_currencies.stdout == '0'"
      changed_when: true
- name: Include FreeBSD installation
  ansible.builtin.include_tasks: installation_freebsd.yml
  when: "ansible_facts.os_family == 'FreeBSD'"


- name: Apply gnuhealth-control template
  ansible.builtin.template:
    src: gnuhealth-control.j2
    dest: /usr/local/bin/gnuhealth-control
    owner: gnuhealth
    group: "{{ 'users' if ansible_facts.os_family == 'Suse' else 'gnuhealth' }}"
    mode: '0744'
- name: Apply trytond cron service template
  ansible.builtin.template:
    src: gnuhealth_cron.service.j2
    dest: /etc/systemd/system/gnuhealth_cron.service
    mode: '0644'
  notify: Restart and enable trytond cron
  when: gh_cron
- name: Create bash script for pypi minor updates
  ansible.builtin.template:
    src: update_gnuhealth_pip_minor.j2
    dest: /usr/local/bin/update_gnuhealth_pip_minor
    mode: '0600'
  when: gh_update_weekly
- name: Create cron job for pypi minor updates
  ansible.builtin.cron:
    name: 'Update GNU Health PyPI minor version'
    job: 'bash /usr/local/bin/update_gnuhealth_pip_minor'
    special_time: 'weekly'
  when: gh_update_weekly
